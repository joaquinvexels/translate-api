const express = require('express')
const { PORT } = require('./constants')

require('dotenv').config()

var app = express()

app.use('/api', require('./api'))

app.listen(PORT, function() {
    console.log(`Start Translate Getty API on port: ${PORT}`)
})