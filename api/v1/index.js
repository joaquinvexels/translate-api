var express = require('express')

var translateController = require('../../controllers/translate.controller')

var router = express.Router()

router.get('/translate/:lang/:str', translateController.translate)

module.exports = router