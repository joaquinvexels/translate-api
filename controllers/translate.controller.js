var translateApi = require('../services/translateApi.service')

const translate = (req, res, next) => {
    var { str, lang } = req.params
    str = str.trim().toLowerCase().replace(/\s{2,}/g, ' ').split(" ")

    translateApi
        .translate(str, lang)
        .then(results => {
            const data = {
                original_string: str,
                language: lang,
                ...results
            }
            res.status(200).send({error: false, data})
        })
        .catch(err => {
            console.log('Error on translate api request search term: ', term, err)
        })
}

module.exports = {translate}