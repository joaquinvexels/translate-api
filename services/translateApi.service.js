var cache = require('./cache.service')
var md5 = require('md5')
var {
    ENGINE,
    GOOGLE_API_KEY,
    YANDEX_API_KEY
} = require('../constants')
var API_KEY = ENGINE == 'google' ? GOOGLE_API_KEY : YANDEX_API_KEY
var translateText = require('translate')


const makeTranslation = (str, lang) => {
    return new Promise((resolve, reject) => {
        translateText(str, {
            from: lang,
            to: 'en',
            engine: ENGINE,
            key: API_KEY
        }).then(translateData => {
            resolve(translateData)
        }).catch(err => {
            reject(err)
        });
    })
}

const translate = (str = "", lang = "en") => {

    return new Promise((resolve, reject) => {

        let results = ""
        let cacheStatus = "hit"

        const getData = () => {
            return Promise.all(str.map(async string => {
                try {
                    let cacheKey = `${md5(string)}_${lang}`
                    let cacheValue = await cache.get(cacheKey)
                    cacheStatus = !cacheValue ? 'miss' : cacheStatus

                    if (!cacheValue) {
                        if (process.env.TRANSLATION == 'true') {

                            try {
                                let result = await makeTranslation(string, lang)

                                if (result.length === 0) {
                                    return string
                                } else {
                                    cache.set(cacheKey, result)
                                    return result
                                }

                            } catch (error) {
                                return string
                            }

                        } else {
                            return string
                        }

                    } else {
                        return cacheValue
                    }

                } catch (err) {
                    return err
                }
            }))
        }

        getData().then(data => {
            results = data.join(" ")
            resolve({
                cache: cacheStatus,
                result: results
            })
        }).catch(err => {
            reject(new Error(err))
        })
    })
}

module.exports = {
    translate
}