const cacheManager = require('cache-manager');
const redisStore = require('cache-manager-redis');
const {
    CACHE_TTL
} = require("../constants")

var redisCache = cacheManager.caching({
    store: redisStore,
    host: 'localhost', // default value
    port: 6379, // default value
    db: 0,
    ttl: CACHE_TTL,
});

var ttl = CACHE_TTL;

// listen for redis connection error event
redisCache.store.events.on('redisError', function (error) {
    // handle error here
    console.log(error);
});

module.exports = {
    clean(key) {
        redisCache.del(key, function (err) {
            if (err) {
                throw err;
            }
            console.log("Cache delete:", key)
        });
    },
    get(key) {
        return new Promise((resolve, reject) => {
            redisCache.get(key, function (err, result) {
                if (err) {
                    throw err;
                }
                resolve(result)
            })
          })
    },
    set(key, value) {
        redisCache.set(key, value, {
            ttl: ttl
        }, function (err) {
            if (err) {
                throw err;
            }
        })
    }
}